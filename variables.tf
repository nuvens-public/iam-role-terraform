##################################################################################
# VARIABLES
##################################################################################

variable "region" {
  description = "AWS Location to use by default"
  default     = "eu-west-1"
}

variable "vpc_id" {
  description = "AWS VPC to use"
  default     = "vpc-0"
}

variable "wsm_role" {
  description = "Name for the WSM Role"
  default     = "WorkSpacesManagerRole"
}

variable "pricing_pol" {
  description = "Pricing Policy Name"
  default     = "WSMPricingPolicy"
}

variable "costex_pol" {
  description = "Cost Explorer Policy Name"
  default     = "WSMCostExplorerPolicy"
}

variable "ec2_pol" {
  description = "EC2 Policy Name"
  default     = "WSMEC2Policy"
}

variable "s3_pol" {
  description = "S3 Policy Name"
  default     = "WSMS3Policy"
}

variable "cw_pol" {
  description = "CloudWatch Policy Name"
  default     = "WSMCloudwatchPolicy"
}

variable "euc_pol" {
  description = "EUC Policy Name"
  default     = "WSMEUCPolicy"
}

variable "secrets_pol" {
  description = "Secrets Manager Policy Name"
  default     = "WSMSecretsPolicy"
}

variable "sg_name" {
  description = "EC2 Security Group Name"
  default     = "SG-WorkSpacesManager"
}

variable "ingress_cidr_blocks" {
  description = "Internal CIDR to access WSM"
  default     = "0.0.0.0/0"
}

variable "egress_cidr_blocks" {
  description = "Outbound CIDR to access WSM"
  default     = "0.0.0.0/0"
}
