##################################################################################
# IAM POLICY
##################################################################################

# Terraform's "jsonencode" function converts a
# Terraform expression result to valid JSON syntax.

resource "aws_iam_policy" "pricing_policy" {
  name        = var.pricing_pol
  path        = "/"
  description = var.pricing_pol
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "pricing:*"
        ]
        Effect   = "Allow"
        Resource = "*"
      },
    ]
  })
}

resource "aws_iam_policy" "costexplorer_policy" {
  name        = var.costex_pol
  path        = "/"
  description = var.costex_pol
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "ce:*"
        ]
        Effect   = "Allow"
        Resource = "*"
      },
    ]
  })
}

resource "aws_iam_policy" "ec2_policy" {
  name        = var.ec2_pol
  path        = "/"
  description = var.ec2_pol
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "ec2:Describe*",
          "elasticloadbalancing:Describe*",
          "autoscaling:Describe*"
        ]
        Effect   = "Allow"
        Resource = "*"
      },
    ]
  })
}

resource "aws_iam_policy" "s3_policy" {
  name        = var.s3_pol
  path        = "/"
  description = var.s3_pol
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "s3:GetLifecycleConfiguration",
          "s3:GetBucketTagging",
          "s3:GetInventoryConfiguration",
          "s3:GetObjectVersionTagging",
          "s3:ListBucketVersions",
          "s3:GetBucketLogging",
          "s3:ListBucket",
          "s3:GetAccelerateConfiguration",
          "s3:GetBucketPolicy",
          "s3:GetObjectVersionTorrent",
          "s3:GetObjectAcl",
          "s3:GetEncryptionConfiguration",
          "s3:GetBucketRequestPayment",
          "s3:GetObjectVersionAcl",
          "s3:GetObjectTagging",
          "s3:GetMetricsConfiguration",
          "s3:GetBucketPublicAccessBlock",
          "s3:GetBucketPolicyStatus",
          "s3:ListBucketMultipartUploads",
          "s3:GetBucketWebsite",
          "s3:GetBucketVersioning",
          "s3:GetBucketAcl",
          "s3:GetBucketNotification",
          "s3:GetReplicationConfiguration",
          "s3:ListMultipartUploadParts",
          "s3:GetObject",
          "s3:GetObjectTorrent",
          "s3:GetBucketCORS",
          "s3:GetAnalyticsConfiguration",
          "s3:GetObjectVersionForReplication",
          "s3:GetBucketLocation",
          "s3:GetObjectVersion"
        ],
        Resource = [
          "arn:aws:s3:::workspacescostoptimizer-costoptimizerbucket*/*"
        ],
        Effect = "Allow",
        Sid    = "VisualEditor0"
      },
      {
        Action = [
          "s3:GetAccountPublicAccessBlock",
          "s3:ListAllMyBuckets"
        ],
        Resource = "*",
        Effect   = "Allow",
        Sid      = "VisualEditor1"
      },
    ]
  })
}

resource "aws_iam_policy" "cloudwatch_policy" {
  name        = var.cw_pol
  path        = "/"
  description = var.cw_pol
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "cloudwatch:DescribeAlarmHistory",
          "cloudwatch:DescribeAlarmsForMetric",
          "cloudwatch:DescribeAlarms",
          "cloudwatch:Describe*",
          "cloudwatch:GetDashboard",
          "cloudwatch:GetMetricData",
          "cloudwatch:GetMetricStatistics",
          "cloudwatch:GetMetricWidgetImage",
          "cloudwatch:ListMetrics",
          "kms:ListKeys",
          "kms:ListAliases",
          "kms:DescribeKey",
          "logs:GetLogEvents",
          "logs:FilterLogEvents",
          "logs:GetLogGroupFields",
          "logs:GetQueryResults",
          "logs:GetLogDelivery",
          "logs:GetLogRecord",
          "logs:StartQuery",
          "logs:StopQuery",
          "logs:TestMetricFilter"
        ],
        Resource = "*",
        Effect   = "Allow",
        Sid      = "VisualEditor0"
      },
    ]
  })
}

resource "aws_iam_policy" "euc_policy" {
  name        = var.euc_pol
  path        = "/"
  description = var.euc_pol
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "appstream:*",
          "ds:CheckAlias",
          "ds:CreateAlias",
          "ds:DescribeDirectories",
          "kms:ListKeys",
          "kms:ListAliases",
          "kms:DescribeKey",
          "workspaces:*"
        ],
        Resource = "*",
        Effect   = "Allow",
        Sid      = "VisualEditor0"
      },
    ]
  })
}

resource "aws_iam_policy" "secrets_policy" {
  name        = var.secrets_pol
  path        = "/"
  description = var.secrets_pol
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "secretsmanager:GetSecretValue",
          "secretsmanager:DescribeSecret"
        ],
        Resource = "*",
        Effect   = "Allow",
        Sid      = "VisualEditor0"
      },
    ]
  })
}