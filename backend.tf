##################################################################################
# GITLAB BACKEND
##################################################################################
# Backend is hosted in the CloudShell Session in AWS, but it can be specified if needed

# Sample Using GitLab Backend
#terraform {
#  backend "http" {
#    address         = "https://gitlab.com/api/v4/projects/00000000/terraform/state/tfstate"
#    lock_address    = "https://gitlab.com/api/v4/projects/00000000/terraform/state/tfstate/lock"
#    unlock_address  = "https://gitlab.com/api/v4/projects/00000000/terraform/state/tfstate/lock"
#  }
#}

# Sample Terraform Cloud Backend
#terraform {
#  backend "remote" {
#    organization = "0000000000000-corp"
#    workspaces {
#      name = "0000000000000-app"
#    }
#  }
#}

# Sample Using S3 + DynamoDB Backend
#terraform {
#  backend "s3" {
#    bucket         = "0000000000000-tfstate"
#    key            = "project000.tfstate"
#    region         = "eu-west-1"
#    dynamodb_table = "0000000000000_tflockid"
#  }
#}

##################################################################################
# DATA
##################################################################################

data "aws_availability_zones" "available" {}

data "aws_caller_identity" "current" {}

data "aws_region" "current" {}