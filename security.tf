##################################################################################
# SECURITY GROUPS
##################################################################################

resource "aws_security_group" "sg_ec2" {
  name        = var.sg_name
  description = "Security Group for WorkSpaces Manager"
  vpc_id      = var.vpc_id
  ingress {
    description = "Allow HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = [var.ingress_cidr_blocks]
  }
  ingress {
    description = "Allow HTTPS"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = [var.ingress_cidr_blocks]
  }
  ingress {
    description = "Allow MS-SQL"
    from_port   = 1433
    to_port     = 1433
    protocol    = "tcp"
    cidr_blocks = [var.ingress_cidr_blocks]
  }
  ingress {
    description = "Allow RDP"
    from_port   = 3389
    to_port     = 3389
    protocol    = "tcp"
    cidr_blocks = [var.ingress_cidr_blocks]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [var.egress_cidr_blocks]
  }
  tags = {
    Name = var.sg_name
  }
}
