# IAM-Role-Terraform

IAM Role, Instance Profile and Policies for WorkSpaces Manager with Terraform


## Description
This project will create all the necessary elements for WorkSpaces Manager to access the AWS APIs for it to work. Basically, it creates the following elements in the AWS account:
1) IAM Role as `WorkSpacesManagerRole`
2) EC2 Instance Profile as `WorkSpacesManagerRole`
3) Attach AWS Managed Policy `AmazonSSMManagedInstanceCore` (replaces former AWS managed policy called `AmazonEC2RoleforSSM`) that allows to use Systems Manager Session Manager to connect to the instance
4) Create and attach a policy as `WSMPricingPolicy` to access AWS Pricing elements
5) Create and attach a policy as `WSMCostExplorerPolicy` to access AWS Cost Explorer and Cost Optimizer features
6) Create and attach a policy as `WSMS3Policy` to read from the Cost Optimizer Bucket
7) Create and attach a policy as `WSMEC2Policy` to read from EC2 Compute
8) Create and attach a policy as `WSMCloudwatchPolicy` to read from CloudWatch
9) Create and attach a policy as `WSMEUCPolicy` to read from AppStream, Directory Services (AD Connectors also) and WorkSpaces services
10) Create and attach a policy as `WSMSecretsPolicy` to read from Secrets Manager
11) Create a security group in the specified region and vpc as `Security Group for WorkSpaces Manager`, from the variable `sg_name`. It also requires `region` and `vpc_id` to be defined.


## Editing variables
There are some variables that can be edited for us to modify the names showed above in the description section in the file `varaibles.tf`. We can rename the IAM Role And Instance Profile with the variable `wsm_role`.

In this file, you can also manage other variable such as `region`, `sg_name` and `vpc_id`, which are in use in this project for the creation of a security group that will be used by WSM. It is not needed by the rest of the IAM resources, which are global.

The policy names can also be adjusted in the same file and are assigned to the following variables:
* `pricing_pol`
* `costex_pol`
* `ec2_pol`
* `s3_pol`
* `cw_pol`
* `euc_pol`
* `secrets_pol`


## Create a Cloud Shell Session in AWS in the region to deploy
```bash
sudo yum install -y yum-utils nano
sudo yum-config-manager --add-repo https://rpm.releases.hashicorp.com/AmazonLinux/hashicorp.repo
sudo yum -y install terraform
terraform version
```


## Add the basics to our profile
```bash
nano ~/.bash_profile
```

Add lines:
```yaml
sudo yum install -y yum-utils nano
sudo yum install -y bash-completion > /dev/null 2>&1
```


## If we want to create a persistent directory and move binaries there - don't do it if you have not used it before!!
```bash
mkdir -p $HOME/.local/bin
cd $HOME/.local/bin
sudo mv /usr/bin/nano $HOME/.local/bin
sudo mv /usr/bin/terraform $HOME/.local/bin
```


## Execute Terraform commands
```bash
mkdir -p ~/tf
cd ~/tf
git clone https://gitlab.com/nuvens-public/iam-role-terraform.git
cd iam-role-terraform
terraform fmt -list=true -diff -write=false -recursive .
terraform init
terraform plan -out wsm_roles.tfplan
terraform apply wsm_roles.tfplan
```
