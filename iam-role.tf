##################################################################################
# IAM ROLE
##################################################################################

resource "aws_iam_role" "iam_ssm_role" {
  name = var.wsm_role

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_instance_profile" "iam_instance_role" {
  name = var.wsm_role
  role = aws_iam_role.iam_ssm_role.name
}

resource "aws_iam_role_policy_attachment" "iam_ssm_attach" {
  depends_on = [aws_iam_role.iam_ssm_role]
  role       = aws_iam_role.iam_ssm_role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
}

resource "aws_iam_role_policy_attachment" "pricing_attach" {
  depends_on = [aws_iam_role.iam_ssm_role]
  role       = aws_iam_role.iam_ssm_role.name
  policy_arn = aws_iam_policy.pricing_policy.arn
}

resource "aws_iam_role_policy_attachment" "costexplorer_attach" {
  depends_on = [aws_iam_role.iam_ssm_role]
  role       = aws_iam_role.iam_ssm_role.name
  policy_arn = aws_iam_policy.costexplorer_policy.arn
}

resource "aws_iam_role_policy_attachment" "ec2_attach" {
  depends_on = [aws_iam_role.iam_ssm_role]
  role       = aws_iam_role.iam_ssm_role.name
  policy_arn = aws_iam_policy.ec2_policy.arn
}

resource "aws_iam_role_policy_attachment" "s3_attach" {
  depends_on = [aws_iam_role.iam_ssm_role]
  role       = aws_iam_role.iam_ssm_role.name
  policy_arn = aws_iam_policy.s3_policy.arn
}

resource "aws_iam_role_policy_attachment" "cloudwatch_attach" {
  depends_on = [aws_iam_role.iam_ssm_role]
  role       = aws_iam_role.iam_ssm_role.name
  policy_arn = aws_iam_policy.cloudwatch_policy.arn
}

resource "aws_iam_role_policy_attachment" "euc_attach" {
  depends_on = [aws_iam_role.iam_ssm_role]
  role       = aws_iam_role.iam_ssm_role.name
  policy_arn = aws_iam_policy.euc_policy.arn
}

resource "aws_iam_role_policy_attachment" "secrets_attach" {
  depends_on = [aws_iam_role.iam_ssm_role]
  role       = aws_iam_role.iam_ssm_role.name
  policy_arn = aws_iam_policy.secrets_policy.arn
}